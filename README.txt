Overview
Provide an organized way to view the codes directly on the website.
Useful when

1. you develep a module or theme under 'ALL' website and can run the code under one website and check the code on another when errors occur.
2. study core/contributed modules/themes
3. know where the module located

Features:

View modules and theme code directly on the website
Provision of module path so you can know which module you are using (module package have several modules and some times you don't know which module belong to which package).
The module path and link for viewing the module code is on the module administration page and after the description.
The theme path and link for viewing the theme code is at the bottom of theme administration page (under administration theme section, I'm trying put them after each theme)

Coming feature:
Integration with GeSHi filter. (Thank ELC).
I will make each function collapsible.
More organized folder view.
View any files code by enter the path and file name
And integrate the file viewer with the error message so the error will provide a code viewer link to the problem file.
Code editor.

You may find it helpful when

1. you want to read the code on an iPad, iPhone or any internet accessible devices when you are on the bus or in the bathroom.
2. want to read the code but not go through all the folders to get that file
3. want a separate window to view code from writing code.
4. want to clean up your useless modules but don't know which folder you should delete
5. you love reading code with orz Code Viewer

Made by orzStudio.com (under construction)
More orz development tools are coming.